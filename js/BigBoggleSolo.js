//dice configurations
//https://boardgames.stackexchange.com/questions/29264/boggle-what-is-the-dice-configuration-for-boggle-in-various-languages


//declarations
const DicesFromBoggleSuper ='AAEEGNABBJOOACHOPSAFFKPSAOOWTTCIMOTUDEILRXDELRVYDISTTYEEGHNWEEINSVEHRTVWEIOSSTELTTRYHIMNQUHLNNRZAAEEGNACHOPSAFFKPSDEILRXDEILRVYEEGHNWEIOSSTHIMNQUHLNNRZ';
let board=new Array;

//---------------------------------------------------------------------------
//load highscores
document.getElementsByTagName("body").onload = get_scores(list_scores);
//--------------------------------------------------
//Main Loop
let j = 0;
let C = 0;
let row = new Array();
for(let i=0; i<25 ; i++)
{
    let dice=document.getElementById(i); //get div

    let shuffleEveryDice= shuffleDices(DicesFromBoggleSuper); //shuffle dice letters for every dice
    let shuffleBetweenPlaces = shufflePlaces(shuffleEveryDice)

    dice.innerText = shuffleBetweenPlaces[i];

    dice.classList.add(randomRotation());

    
    //store in board array
 
         

    if(j<=4)
    {   
        row[j]= dice.innerText;
        j++;
    }
    if(j==5)
    { 
        board[C] = row;
        j = 0;
        row=[];
        C++;
    }   
    
    
}

//---------------------------------------------------------------------------
//functions

//this function will shuffle every dice randomly
function shuffleDices(diceCharacters)
{
    //6 characters on 1 dice so cut string after 6 charcters
   let dice = new Array();
   let i= 0;
    for(let j=0; j<25 ; j++)
    {   
            let temp = diceCharacters.slice(i,i+6) 
            let result = temp.charAt(Math.floor(Math.random() * 6));
            dice[j] = result;  
            i=i+6;    
    }

    return dice;
}





//this function wil take the results from shuffle dices and change the places of the dices
function shufflePlaces(dicesCharacters)
{
        for(var i=0 ; i<25-1 ; ++i) 
        {
          let j = Math.floor(Math.random() * 25);;       
          let temp = dicesCharacters[i];             // Swap arr[i] and arr[j]
          dicesCharacters[i] = dicesCharacters[j];
          dicesCharacters[j] = temp;
        }
        
        result = dicesCharacters.join('');                // Convert Array to string
        return result;                        // Return shuffled string
 }
      


 function randomRotation()
 {
     let rNr = Math.floor(Math.random() * 4) + 1;
     let result = '';
     
     switch (rNr)
     {
         case 1 : result ="rot1"; break;
         case 2 : result ="rot2"; break;
         case 3 : result ="rot3"; break;
         case 4 : result ="rot4"; break;
     }
     return result;
 }
 
 console.log(randomRotation());

//-------------------
//buttons
const btnStart = document.getElementById("startBtn");
const btnSolve = document.getElementById("solveBtn");
const btnNewGame = document.getElementById("newGame");
const btnConfirm = document.getElementById("confirm");
const btnClear = document.getElementById("clear");

const resultSolver=document.getElementById("solveResults");
const solvebox = document.getElementById("solvebox");
const box=document.getElementById("box");

//hide elements
time.style.display = 'none';
btnNewGame.style.display='none';
btnSolve.style.display='none';
solvebox.style.display='none';



btnStart.addEventListener("click",fadeBox);

function fadeBox()
{
    box.classList.toggle('hide');
    if(btnStart.innerText != 'Correction')
    {
        startTimer();  
    }
    btnStart.style.display = 'none'; //hide
    time.style.display = 'inline-block';
    box.style.zIndex = "-1";
}

function end()
{
    box.classList.toggle('hide');
    btnStart.style.display ='inline-block';
    btnStart.innerHTML="Correction";
    btnNewGame.style.display='inline-block';
    time.style.display = 'none';
    btnSolve.style.display ='inline-block';
    solvebox.style.display ='inline-block';
    btnConfirm.style.display ='none';
    btnClear.style.display ='none';
    box.style.zIndex = "1";

    //check if words are in dictionary return array with true or false
    let corectionArray = compareAnswersToDictionary( finalWords ,dict);

    //remove duplicates
    let removedDuplicates = checkDuplicates(finalWords,score,corectionArray);

    //remove words that are not in the dictionary
    let removedWords = cleanPlayerWordsArray(finalWords, corectionArray, score);

    removedFinal = removedWords.concat(removedDuplicates) ;
    console.log("removed "+removedFinal);
    //create list with removed words
    wordBoxPlayer.insertAdjacentHTML('afterbegin', "Next words are invalid ore double and removed from list :<br> <s class='darkRed'> " + removedFinal + "</s><br>");

    let totalScore = pointsSum(score);
    wordBoxPlayer.insertAdjacentHTML('afterbegin',"total score = <span class='steelblue2'>" + totalScore + "</span><br>");
}

btnNewGame.addEventListener("click",restart);

function restart()
{
    location.reload();
}

btnSolve.addEventListener("click",solveRiddle);
function solveRiddle()
{     
    let boggle = board ;
    let outputTemp = [];
	
	findWords(boggle, root);

  /*   let tempOuput = [];

    for(let i=0; i< resultBoggle.length; i++)
    {
        tempOuput = resultBoggle.splice(i,0,directionResult[i]);
        
    } */
    for(let i=0; i< resultBoggle.length; i++)
    {
        let temp= resultBoggle[i];
        let PartA = "<a href='https://www.vandale.nl/gratis-woordenboek/nederlands/betekenis/";
        let PartB = PartA + temp + "' target='_blank' rel='noopener noreferrer'>" + temp + "</a>";
        
        outputTemp.push(PartB);
    }
    outputTemp.sort();
    output = outputTemp.join('<br>');
    
    resultSolver.innerHTML="We found <span class='steelblue2'>"+ wordCounter +"</span><br> words in the dictionary <br>" + output;
    console.log("results "+ resultBoggle);
}


