//read highscore 
const Errors = document.getElementById('error');
let fname= [];
let fscores= [];
let lowest_score = 0;

fname[0]= document.getElementById("N1");
fname[1]= document.getElementById("N2");
fname[2]= document.getElementById("N3");
fname[3]= document.getElementById("N4");
fname[4]= document.getElementById("N5");
fname[5]= document.getElementById("N6");
fname[6]= document.getElementById("N7");
fname[7]= document.getElementById("N8");
fname[8]= document.getElementById("N9");
fname[9]= document.getElementById("N10");

fscores[0] = document.getElementById("S1");
fscores[1] = document.getElementById("S2");
fscores[2] = document.getElementById("S3");
fscores[3] = document.getElementById("S4");
fscores[4] = document.getElementById("S5");
fscores[5] = document.getElementById("S6");
fscores[6] = document.getElementById("S7");
fscores[7] = document.getElementById("S8");
fscores[8] = document.getElementById("S9");
fscores[9] = document.getElementById("S10");



function get_scores (callback) {
  // High Score Data
  let file = "/highscore/highscore.json";

  // Fetch High Score Data
  fetch(file, {cache: 'no-cache'})
    .then(function(response) 
    {
        //  If the response isn't OK
        if (response.status !== 200) 
        {
          Errors.innerHTML = response.status;
        }
        // If the response is OK
        response.json().then(function(data) 
        {
          let scores = JSON.stringify(data);
          console.log(scores);
          callback (scores);
        });
    })
    // If there is an error
    .catch(function(err) 
    {
      Errors.innerHTML = err;
    });
}


const List = document.getElementById("highscores");

var list_scores = function (scores) 
{
  // turn scores JSON to a JavaScript object
  let object = JSON.parse(scores);

  // Store lowest high score for later
  lowest_score = object[9].score;
  document.getElementById('lowscore').value = lowest_score;
  for (let i=0; i<10; i++)
  {
    fname[i].innerHTML = object[i].name;
    fscores[i].innerHTML = object[i].score;
  }

}

/* 
if(totalScore>fscores[9])
{

} */




const Myform = document.getElementById("myform");
Myform.addEventListener("submit", function (event) 
{
  // don't reload page
  event.preventDefault();
  var tenth_score = document.getElementById('lowscore').value;

  
      //Form Data Object
    var formData = new FormData(this);

    // fetch request
    fetch ("highscore.php", 
    {
      method: "post",
      body: formData
    })
    .then (function (response)
    {
      return response.text();
    })
    .then(function(text) 
    {
      
      console.log(text);
    })
    .catch(function (err) 
    {
      Errors.innerHTML = err;
    })
});