//https://www.geeksforgeeks.org/boggle-set-2-using-trie/
// Javascript program for Boggle game
 
// Alphabet size
let SIZE = 26;
let M = 5;
let N = 5;

let wordCounter= 0;
resultBoggle = [];
directions="";
directionResult=[];
dict=[];

 
 // trie Node
class TrieNode
{
    constructor()
    {
        this.leaf=false;
        this.Child = new Array(SIZE);
        for (let i = 0; i < SIZE; i++)
        {
            this.Child[i]=null;
        }
           
    }
}
 
// If not present, inserts a key into the trie
    // If the key is a prefix of trie node, just
    // marks leaf node
function insert(root,Key)
{
    let n = Key.length;
        let pChild = root;
   
        for (let i = 0; i < n; i++) 
        {
            let index = Key[i].charCodeAt(0) - 'A'.charCodeAt(0);
   
            if (pChild.Child[index] == null)
                pChild.Child[index] = new TrieNode();
   
            pChild = pChild.Child[index];
        }
   
        // make last node as leaf node
        pChild.leaf = true;
}
 
// function to check that current location
    // (i and j) is in matrix range
function isSafe(i,j,visited)
{
    return (i >= 0 && i < M && j >= 0 && j < N && !visited[i][j]);
}
 
// A recursive function to print
    // all words present on boggle
function searchWord(root,boggle,i,j,visited,str )
{
    // if we found word in trie / dictionary
        if (root.leaf == true)
        {
            //document.getElementById("solveResults").insertAdjacentHTML('beforeend', str+" <br>");
            wordCounter++;
            resultBoggle.push(str);
            //directionResult.push(directions);
        }
            
   
        // If both I and j in  range and we visited
        // that element of matrix first time
        if (isSafe(i, j, visited)) 
        {
            // make it visited
            visited[i][j] = true;
   
            // traverse all child of current root
            for (let K = 0; K < SIZE; K++) 
            {
                if (root.Child[K] != null) 
                {
                    // current character
                    let ch = String.fromCharCode(K + 'A'.charCodeAt(0));
                   
                    // Recursively search reaming character of word
                    // in trie for all 8 adjacent cells of
                    // boggle[i][j]
                    if (isSafe(i + 1, j + 1, visited) && boggle[i + 1][j + 1] == ch)
                    {
                        searchWord(root.Child[K], boggle,  i + 1, j + 1, visited, str + ch);
                        //directions += "↘";    
                    }
                        
                    if (isSafe(i, j + 1, visited) && boggle[i][j + 1] == ch)
                    {
                        searchWord(root.Child[K], boggle, i, j + 1, visited, str + ch);
                        //directions += "→";
                        
                    }
                        
                    if (isSafe(i - 1, j + 1, visited) && boggle[i - 1][j + 1] == ch)
                    {
                        searchWord(root.Child[K], boggle, i - 1, j + 1, visited, str + ch);
                        //directions += "↗";
                        
                    }
                        
                    if (isSafe(i + 1, j, visited) && boggle[i + 1][j] == ch)
                    {
                        searchWord(root.Child[K], boggle, i + 1, j, visited, str + ch);
                        //directions += "↓";
                    }
                        
                    if (isSafe(i + 1, j - 1, visited) && boggle[i + 1][j - 1] == ch)
                    {
                        searchWord(root.Child[K], boggle, i + 1, j - 1, visited, str + ch);
                        //directions += "↙";
                    }
                        
                    if (isSafe(i, j - 1, visited) && boggle[i][j - 1] == ch)
                    {
                        searchWord(root.Child[K], boggle, i, j - 1, visited, str + ch);
                        //directions +=  "←";
                    }
                        
                    if (isSafe(i - 1, j - 1, visited) && boggle[i - 1][j - 1] == ch)
                    {
                        searchWord(root.Child[K], boggle, i - 1, j - 1, visited, str + ch);
                        //directions += "↖";
                    }
                        
                    if (isSafe(i - 1, j, visited) && boggle[i - 1][j] == ch)
                    {
                        searchWord(root.Child[K], boggle, i - 1, j, visited, str + ch);
                        //directions += "↑";
                    }
                       
                }
            }
   
            // make current element unvisited
            visited[i][j] = false;
        }
}
 
// Prints all words present in dictionary.
function findWords(boggle,root)
{
    
    // Mark all characters as not visited
        let visited = new Array(M);
        for(let i=0;i<M;i++)
        {
            visited[i] = new Array(N);

            for(let j=0;j<N;j++)
            {
                visited[i][j]=false;
            }
        }
        let pChild = root;
   
        let str = "";
   
        // traverse all matrix elements
        for (let i = 0; i < M; i++) 
        {
            for (let j = 0; j < N; j++) 
            {
                // we start searching for word in dictionary
                // if we found a character which is child
                // of Trie root
                if (pChild.Child[(boggle[i][j]).charCodeAt(0) - 'A'.charCodeAt(0)] != null) 
                {
                    str = str + boggle[i][j];
                    searchWord(pChild.Child[(boggle[i][j]).charCodeAt(0) - 'A'.charCodeAt(0)], boggle, i, j, visited, str);
                    str = "";
                }
            }
        }
        
}
 
// Dictionary control
//read dictionary and store it in array
fetch('./dict/nl.txt')
  .then(response => response.text())
  .then(data => {
  	let rawDict = data.split("\r"); //split on every row from text
    let cleanDict = []
    for (i = 1; i < rawDict.length; i++)  //remove unwanted \ first character
    {
        cleanDict.push(rawDict[i].substr(1));
    }

    let capDict = cleanDict.map(name => name.toUpperCase()); //transform every word in the array to cappitals
    

    let dictionary = capDict.filter(function(item) //remove only words with lettercount 3 or more
    { 
        return item.length > 2;
    });
     
    dict = dictionary;
    // insert all words of dictionary into trie
    let n = dictionary.length;
    for (let i = 0; i < n; i++)
    {
        insert(root, dictionary[i]);
        if(n == dictionary.length-1)
        {
             console.log("dictionary loaded");
        }
    } 
  	//console.log(dictionary);
  });
//let dictionary=["EEN", "DAS", "AAI", "MES","EEN","NEE","GAAI","ROT" ,"NAT" , "NET", "TEN","DAN" , "HEL", "NET", "AAI","WEG","HAAG","AAG" , "SAAI","TOR" , "TIP", "PIT"]; 



// root Node of trie
let root = new TrieNode();


 
    
 

 
 
