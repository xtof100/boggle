//dice configurations
//https://boardgames.stackexchange.com/questions/29264/boggle-what-is-the-dice-configuration-for-boggle-in-various-languages
//https://boardgamegeek.com/thread/1071406/looking-letter-distribution
//points:https://en.wikipedia.org/wiki/Boggle

//declarations
const DicesFromSuperBigBoggle ='AAAFRSAAEEEEAAEEOOAAFIRSABDEIOADENNNAEEEEMAEEGMUAEGMNNAEILMNAEINOUAFIRSYBBJKXZCCENSTCDDLNNCEIITTCEIPSTCFGNUYDDHNOTDHHLORDHHNOWDHLNOREHILRSEIILSTEILPSTEIO■■■EMTTTOENSSSUGORRVWHIRSTVHOPRSTIPRSYYNOOTUWOOOTTUJKQWXZ';
const SpecialDiceEnglish = ["An","Er","He","In","Qu","Th"];
//const SpecialDiceDutch = [sc,ng,]
//---------------------------------------------------------------------------
//Main Loop

    

    let shuffleEveryDice= shuffleDices(DicesFromSuperBigBoggle); //shuffle dice letters for every dice
    console.log("shuffle every dice" + shuffleEveryDice);

    let shuffleSpecial = shuffleSpecialDice(SpecialDiceEnglish);
    console.log("shuffle special dice" + shuffleSpecial);

    let shuffeledDices = shuffleEveryDice.concat(shuffleSpecial);
    
    shuffeledDices.splice(35, 1);
    console.log("shuffled dices" + shuffeledDices);

    let shuffleBetweenPlaces = shufflePlaces(shuffeledDices)
    console.log("placed" + shuffleBetweenPlaces);
    for(let i=0; i<36 ; i++)
{
    let dice=document.getElementById(i); //get div
    dice.innerHTML = shuffleBetweenPlaces[i];

    dice.classList.add(randomRotation());
}

//---------------------------------------------------------------------------
//functions

//this function will shuffle every dice randomly
function shuffleDices(diceCharacters)
{
    //6 characters on 1 dice so cut string after 6 charcters
   let dice = new Array();
   let i= 0;
    for(let j=0; j<36 ; j++)
    {   
            let temp = diceCharacters.slice(i,i+6) 
            let result = temp.charAt(Math.floor(Math.random() * 6));
            dice[j] = result;  
            i=i+6;    
    }

    return dice;
}

function shuffleSpecialDice(dice)
{
    //6 characters double characters on 1 dice     
            let result = dice[Math.floor(Math.random()*6)];
            return result;
}




//this function wil take the results from shuffle dices and change the places of the dices
function shufflePlaces(dicesCharacters)
{
        for(var i=0 ; i<36-1 ; ++i) 
        {
          let j = Math.floor(Math.random() * 36);;       
          let temp = dicesCharacters[i];             // Swap arr[i] and arr[j]
          dicesCharacters[i] = dicesCharacters[j];
          dicesCharacters[j] = temp;
        }
        
        //result = dicesCharacters.join('');                // Convert Array to string
        return dicesCharacters;                        // Return shuffled string
 }
      


 function randomRotation()
 {
     let rNr = Math.floor(Math.random() * 4) + 1;
     let result = '';
     
     switch (rNr)
     {
         case 1 : result ="rot1"; break;
         case 2 : result ="rot2"; break;
         case 3 : result ="rot3"; break;
         case 4 : result ="rot4"; break;
     }
     return result;
 }
 
 console.log(randomRotation());

