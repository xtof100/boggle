//dice configurations
//https://boardgames.stackexchange.com/questions/29264/boggle-what-is-the-dice-configuration-for-boggle-in-various-languages
//https://boardgamegeek.com/thread/300883/letter-distribution


//declarations
//every 6 characters is a new dice from the game
const DicesFromEightyFive ='ENUJGYAEIWOAKZTONEGWULERRIMSAOHEFISEKXAIFRENADVZEHISRNLIBATNTEVIGNLTPNUEHERASCNTDSEOOMDQBJDCMPAE';
//---------------------------------------------------------------------------
//Main Loop
for(let i=0; i<16 ; i++)
{
    let dice=document.getElementById(i); //get div

    let shuffleEveryDice= shuffleDices(DicesFromEightyFive); //shuffle dice letters for every dice
    let shuffleBetweenPlaces = shufflePlaces(shuffleEveryDice)

    dice.innerText = shuffleBetweenPlaces[i];

    dice.classList.add(randomRotation());
}

//---------------------------------------------------------------------------
//functions

//this function will shuffle every dice randomly
function shuffleDices(diceCharacters)
{
    //6 characters on 1 dice so cut string after 6 charcters
   let dice = new Array();
   let i= 0;
    for(let j=0; j<16 ; j++)
    {   
            let temp = diceCharacters.slice(i,i+6) 
            let result = temp.charAt(Math.floor(Math.random() * 6));
            dice[j] = result;  
            i=i+6;    
    }

    return dice;
}





//this function wil take the results from shuffle dices and change the places of the dices
function shufflePlaces(dicesCharacters)
{
        for(var i=0 ; i<16-1 ; ++i) 
        {
          let j = Math.floor(Math.random() * 16);;       
          let temp = dicesCharacters[i];             // Swap arr[i] and arr[j]
          dicesCharacters[i] = dicesCharacters[j];
          dicesCharacters[j] = temp;
        }
        
        result = dicesCharacters.join('');                // Convert Array to string
        return result;                        // Return shuffled string
 }
      


 function randomRotation()
 {
     let rNr = Math.floor(Math.random() * 4) + 1;
     let result = '';
     
     switch (rNr)
     {
         case 1 : result ="rot1"; break;
         case 2 : result ="rot2"; break;
         case 3 : result ="rot3"; break;
         case 4 : result ="rot4"; break;
     }
     return result;
 }
 
 console.log(randomRotation());


//-------------------
//buttons
const btnStart = document.getElementById("startBtn");
const btnNewGame = document.getElementById("newGame");
const box=document.getElementById("box")

time.style.display = 'none';
btnNewGame.style.display='none';
btnStart.addEventListener("click",fadeBox);

function fadeBox()
{
    box.classList.toggle('hide');
    startTimer();  
    btnStart.style.display = 'none'; //hide
    time.style.display = 'block';
}

function end()
{
    box.classList.toggle('hide');
    btnStart.style.display = 'block';
    btnStart.innerHTML="Correction";
    btnNewGame.style.display='block';
}

btnNewGame.addEventListener("click",restart);

function restart()
{
    location.reload();
}


//-------------------------------------------
//Alternative calculation
/* function randomLetter(diceCharacters) 
{
    let result           = '';
    //boggle dice algorithme 1985
    
    let characters       = diceCharacters;
    let charactersLength = 96;
    
      result = characters.charAt(Math.floor(Math.random() * charactersLength));
   
   return result;
} */
