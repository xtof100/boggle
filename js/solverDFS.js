// Depth First Traversal
//https://www.geeksforgeeks.org/boggle-find-possible-words-board-characters/

	// JavaScript program for Boggle game
	// Let the given dictionary be following
	var dictionary = ["EEN", "DAS", "AAI", "MES","EEN","NEE","GAAI","ROT" ,"NAT" , "NET", "TEN","DAN" , "HEL", "NET", "AAI","WEG","HAAG","AAG" , "SAAI","TOR" , "TIP", "PIT"]; ;
	
	var n = dictionary.length;
	var M = 5,
		N = 5;

	// A given function to check if a given string
	// is present in dictionary. The implementation is
	// naive for simplicity. As per the question
	// dictionary is given to us.
	function isWord(str)
	{
	
		// Linearly search all words
		for (var i = 0; i < n; i++) if (str == dictionary[i]) return true;
		return false;
	}

	// A recursive function to print all words present on boggle
	function findWordsUtil(boggle, visited, i, j, str)
	{
	
		// Mark current cell as visited and
		// append current character to str
		visited[i][j] = true;
		str = str + boggle[i][j];

		// If str is present in dictionary,
		// then print it
		if (isWord(str)) document.getElementById("solveResults").innerHTML = str + "<br>";

		// Traverse 8 adjacent cells of boggle[i,j]
		for (var row = i - 1; row <= i + 1 && row < M; row++)
		for (var col = j - 1; col <= j + 1 && col < N; col++)
			if (row >= 0 && col >= 0 && !visited[row][col])
			findWordsUtil(boggle, visited, row, col, str);

		// Erase current character from string and
		// mark visited of current cell as false
		str = "" + str[str.length - 1];
		visited[i][j] = false;
	}

	// Prints all words present in dictionary.
	function findWords(boggle)
	{
	
		// Mark all characters as not visited
		var visited = Array.from(Array(M), () => new Array(N).fill(0));

		// Initialize current string
		var str = "";

		// Consider every character and look for all words
		// starting with this character
		for (var i = 0; i < M; i++)
		for (var j = 0; j < N; j++) findWordsUtil(boggle, visited, i, j, str);
	}

	
	
	
	